package com.gitlab.soshibby.issuereporter.messageappenders.jaeger;

import com.gitlab.soshibby.issuereporter.log.LogStatement;
import com.gitlab.soshibby.issuereporter.messageappender.MessageAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JaegerLinkMessageAppender implements MessageAppender {

    private static final Logger log = LoggerFactory.getLogger(JaegerLinkMessageAppender.class);
    private Config configuration;

    @Override
    public void init(String config) {
        configuration = Config.from(config);
        validateConfiguration(configuration);
    }

    @Override
    public String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatements) {
        log.info("Generating Jaeger link for trace id {}.", logTrigger.getTraceId());
        return "[" + configuration.getLinkText() + "](" + configuration.getUrl() + "/trace/" + logTrigger.getTraceId() + ")";
    }

    private void validateConfiguration(Config config) {
        Assert.notNull(config, "Configuration is null.");
        Assert.hasText(config.getLinkText(), "Link text is null or empty in config.");
        Assert.hasText(config.getUrl(), "Url is null or empty in config.");
    }
}

