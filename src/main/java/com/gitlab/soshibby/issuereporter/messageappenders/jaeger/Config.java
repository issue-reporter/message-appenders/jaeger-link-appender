package com.gitlab.soshibby.issuereporter.messageappenders.jaeger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

@JsonIgnoreProperties("class")
public class Config {
    private static ObjectMapper mapper = new ObjectMapper();
    private String linkText;
    private String url;

    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static Config from(String config) {
        try {
            return mapper.readValue(config, Config.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

